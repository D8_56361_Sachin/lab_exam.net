﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace D8_56361_Sachin
{
    public partial class AddPerson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButAdd_Click(object sender, EventArgs e)
        {
            string mainconn = ConfigurationManager.ConnectionStrings["myconnection"].ConnectionString;
            SqlConnection sqlconn=new SqlConnection(mainconn);
            string sqlquery = "Insert into [dbo].[RationCardTB] (RationNo,PersonName,Age,Address,ContactNo,Status,Role) values" +
                " (@RationNo,@PersonName,@Age,@Address,@ContactNo,@Status,@Role)";
            SqlCommand sqlcomm = new SqlCommand(sqlquery, sqlconn);
            sqlconn.Open();
            sqlcomm.Parameters.AddWithValue("@RationNo", TxtRationNo);
            sqlcomm.Parameters.AddWithValue("@PersonName", TxtPersonName.Text);
            sqlcomm.Parameters.AddWithValue("@Age", TxtAge);
            sqlcomm.Parameters.AddWithValue("@Address", TxtAddress);
            sqlcomm.Parameters.AddWithValue("@ContactNo",TxtContact);
            sqlcomm.Parameters.AddWithValue("@Status", TxtStatus);
            
            sqlconn.Close();

        }
    }
}