CREATE TABLE RationCardTB (
    RationNo int,
	PersonName VARCHAR(255),
    Age int,
	Address VARCHAR(255),
	ContactNo VARCHAR(255),
	Status VARCHAR(255),
    Role VARCHAR(255),
);